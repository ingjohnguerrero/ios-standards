# iOS Developer's tools #

## Git flow ##

* [What is Gitflow?](https://nvie.com/posts/a-successful-git-branching-model/)
* [Attlasian's Gitflow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow)

* [Better git commit messages](https://medium.com/@iamkinansw/better-git-commit-messages-397d4afa66)

## Feature flags development ##

* [Feature flag devleopment explanation](https://launchdarkly.com/blog/feature-flag-driven-development/)

* [Firebase Remote config](https://firebase.google.com/docs/remote-config?hl=es-419)

## Changelog ##

* [keepachangelog](https://keepachangelog.com/en/1.0.0/)

## Clean code approach ##

* [SwiftLint](https://github.com/realm/SwiftLint)
* [SonarQube](https://www.sonarqube.org/)
* [Code quality](https://swifting.io/blog/2016/10/25/26-swiftlint-sonarqube-and-checkmarx-what-else/)

## Documenting code ##

* [Apple's markdown formatting reference](https://developer.apple.com/library/archive/documentation/Xcode/Reference/xcode_markup_formatting_ref/index.html)
* [Swift documentation](https://nshipster.com/swift-documentation/)